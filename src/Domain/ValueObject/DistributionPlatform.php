<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject;

use InvalidArgumentException;

class DistributionPlatform
{
    public const PLATFORM_SITE_NOTIFICATION = "SiteNotification";
    public const PLATFORM_EMAIL_NOTIFICATION = "EmailNotification";

    private string $type;

    /** @throws InvalidArgumentException */
    public function __construct(string $type)
    {
        if (!$this->isValidPlatform($type)) {
            throw new InvalidArgumentException();
        }

        $this->type = $type;
    }

    public function __toString(): string
    {
        return $this->type;
    }

    private function isValidPlatform(string $type): bool
    {
        $allowedPlatforms = [
            self::PLATFORM_SITE_NOTIFICATION,
            self::PLATFORM_EMAIL_NOTIFICATION
        ];

        if (!in_array($type, $allowedPlatforms)) {
            return false;
        }

        return true;
    }
}