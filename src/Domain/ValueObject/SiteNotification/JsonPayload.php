<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\SiteNotification;

use InvalidArgumentException;

class JsonPayload
{
    private string $payload;

    /** @throws InvalidArgumentException */
    public function __construct(string $payload)
    {
        if (is_null(json_decode($payload))) {
            throw new InvalidArgumentException();
        }

        $this->payload = $payload;
    }

    public function __toString(): string
    {
        return $this->payload;
    }
}