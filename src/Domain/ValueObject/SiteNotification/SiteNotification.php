<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\SiteNotification;

use BNNVARA\Notification\Domain\ValueObject\AccountId;

class SiteNotification
{
    private AccountId $accountId;
    private Name $name;
    private \DateTime $dateTime;
    private JsonPayload $payload;

    public function __construct(AccountId $accountId, Name $name, \DateTime $dateTime, JsonPayload $payload)
    {
        $this->accountId = $accountId;
        $this->name = $name;
        $this->dateTime = $dateTime;
        $this->payload = $payload;
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    public function getPayload(): JsonPayload
    {
        return $this->payload;
    }
}