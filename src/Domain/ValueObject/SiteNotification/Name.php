<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\SiteNotification;

use InvalidArgumentException;

class Name
{
    public const NAME_BRAND_SUBSCRIBED = 'SubscribedToBrand';
    public const ARTICLE_CREATED = 'ArticleCreated';
    public const MEDIA_CREATED = 'MediaCreated';

    private string $name;

    /** @throws InvalidArgumentException */
    public function __construct(string $name)
    {
        if (!$this->isValid($name)) {
           throw new InvalidArgumentException();
        }

        $this->name = $name;
    }

    private function isValid(string $status): bool
    {
        $allowedNames = [
            self::NAME_BRAND_SUBSCRIBED,
            self::ARTICLE_CREATED,
            self::MEDIA_CREATED
        ];

        return in_array($status, $allowedNames);
    }

    public function __toString(): string
    {
        return $this->name;
    }
}