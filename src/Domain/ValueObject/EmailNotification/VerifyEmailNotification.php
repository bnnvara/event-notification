<?php

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

class VerifyEmailNotification extends EmailNotification
{
    public const NOTIFICATION_TYPE = "VerifyEmail";

    private string $uuid;
    private string $verificationToken;

    public function __construct(string $uuid, string $emailAddress, string $verificationToken)
    {
        $this->uuid = $uuid;
        $this->emailAddress = $emailAddress;
        $this->type = self::NOTIFICATION_TYPE;
        $this->verificationToken = $verificationToken;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getVerificationToken(): string
    {
        return $this->verificationToken;
    }
}