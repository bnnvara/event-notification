<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

class EditorialEmailNotification extends EmailNotification
{
    public const NOTIFICATION_TYPE = "EditorialNotification";

    protected string $emailAddress;

    private string $userId;
    private string $title;
    private string $slug;
    private string $imageUrl;
    private string $content;
    private string $linkText;
    private string $brandName;
    private string $brandSlug;
    private string $accountEmailOptOutSecret;

    public function __construct(
        string $userId,
        string $title,
        string $slug,
        string $imageUrl,
        string $content,
        string $linkText,
        string $brandName,
        string $brandSlug,
        string $accountEmailOptOutSecret,
        string $emailAddress
    ) {
        $this->userId = $userId;
        $this->title = $title;
        $this->slug = $slug;
        $this->imageUrl = $imageUrl;
        $this->content = $content;
        $this->linkText = $linkText;
        $this->brandName = $brandName;
        $this->brandSlug = $brandSlug;
        $this->accountEmailOptOutSecret = $accountEmailOptOutSecret;
        $this->emailAddress = $emailAddress;
        $this->type = self::NOTIFICATION_TYPE;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getLinkText(): string
    {
        return $this->linkText;
    }

    public function getBrandName(): string
    {
        return $this->brandName;
    }

    public function getBrandSlug(): string
    {
        return $this->brandSlug;
    }

    public function getAccountEmailOptOutSecret(): string
    {
        return $this->accountEmailOptOutSecret;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }
}
