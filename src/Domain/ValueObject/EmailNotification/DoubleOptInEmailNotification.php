<?php

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

class DoubleOptInEmailNotification extends EmailNotification
{
    public const NOTIFICATION_TYPE = "DoubleOptIn";

    private ?string $accountId;
    private string $subscriptionListName;
    private string $subscriptionToken;

    public function __construct(?string $accountId, string $emailAddress, string $subscriptionListName, string $subscriptionToken)
    {
        $this->accountId = $accountId;
        $this->emailAddress = $emailAddress;
        $this->type = self::NOTIFICATION_TYPE;
        $this->subscriptionListName = $subscriptionListName;
        $this->subscriptionToken = $subscriptionToken;
    }

    public function getSubscriptionListName(): string
    {
        return $this->subscriptionListName;
    }

    public function getSubscriptionToken(): string
    {
        return $this->subscriptionToken;
    }

    public function getAccountId(): ?string
    {
        return $this->accountId;
    }
}