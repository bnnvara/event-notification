<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\Publication;
use InvalidArgumentException;

class EditorialBundledEmailNotification extends EmailNotification
{
    public const NOTIFICATION_TYPE = "EditorialBundledNotification";

    protected string $emailAddress;

    private string $userId;
    private array $publications;
    private string $accountEmailOptOutSecret;

    public function __construct(
        string $userId,
        array $publications,
        string $accountEmailOptOutSecret,
        string $emailAddress
    ) {
        $this->userId = $userId;
        $this->accountEmailOptOutSecret = $accountEmailOptOutSecret;
        $this->emailAddress = $emailAddress;
        $this->type = self::NOTIFICATION_TYPE;

        foreach ($publications as $publication) {
            if (get_class($publication) !== Publication::class) {
                throw new InvalidArgumentException(
                    sprintf('publications only accept publications %s given', get_class($publication))
                );
            }
        }
        $this->publications = $publications;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getPublications(): array
    {
        return $this->publications;
    }

    public function getAccountEmailOptOutSecret(): string
    {
        return $this->accountEmailOptOutSecret;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }
}
