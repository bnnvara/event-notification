<?php

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

abstract class EmailNotification
{
    protected string $type;
    protected string $emailAddress;

    public function getType(): string
    {
        return $this->type;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }
}