<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject\EmailNotification;

class EditorialEmailNotificationOptOut
{
    private string $hash;

    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    public function getHash(): string
    {
        return $this->hash;
    }
}