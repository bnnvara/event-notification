<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\Exceptions\InvalidIntervalException;

class Settings
{
    private const NOTIFICATION_INTERVAL = [
        self::DAILY,
        self::NONE
    ];
    private const DAILY = 'Daily';
    private const NONE = 'None';

    private string $accountId;
    private bool $editorialNotificationPerEmail;
    private string $editorialNotificationInterval;

    public function __construct(string $accountId, bool $editorialNotificationPerEmail, string $editorialNotificationInterval)
    {
        $this->accountId = $accountId;
        $this->editorialNotificationPerEmail = $editorialNotificationPerEmail;
        if (!in_array($editorialNotificationInterval, self::NOTIFICATION_INTERVAL)){
            Throw new InvalidIntervalException($editorialNotificationInterval . ' is not a valid interval');
        }
        $this->editorialNotificationInterval = $editorialNotificationInterval;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function hasEditorialNotificationPerEmail(): bool
    {
        return $this->editorialNotificationPerEmail;
    }

    public function getEditorialNotificationInterval(): string
    {
        return $this->editorialNotificationInterval;
    }
}
