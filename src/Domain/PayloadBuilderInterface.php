<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain;

interface PayloadBuilderInterface
{
    public function addParameter(string $parameterName, string $value);
    public function createPayload(): Payload;
}