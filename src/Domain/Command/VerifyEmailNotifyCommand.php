<?php

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\VerifyEmailNotification;

class VerifyEmailNotifyCommand
{
    private VerifyEmailNotification $data;

    public function __construct(VerifyEmailNotification $data)
    {
        $this->data = $data;
    }

    public function getData(): VerifyEmailNotification
    {
        return $this->data;
    }
}