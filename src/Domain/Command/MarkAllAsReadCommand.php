<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\AccountId;
use BNNVARA\Notification\Domain\ValueObject\DistributionPlatform;

class MarkAllAsReadCommand
{
    private AccountId $accountId;
    private DistributionPlatform $distributionPlatform;

    public function __construct(AccountId $accountId, DistributionPlatform $distributionPlatform)
    {
        $this->accountId = $accountId;
        $this->distributionPlatform = $distributionPlatform;
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getDistributionPlatform(): DistributionPlatform
    {
        return $this->distributionPlatform;
    }
}