<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotificationOptOut;

class EditorialEmailNotificationOptOutCommand
{
    private EditorialEmailNotificationOptOut $data;

    public function __construct(EditorialEmailNotificationOptOut $editorialEmailNotificationOptOut)
    {
        $this->data = $editorialEmailNotificationOptOut;
    }

    public function getData(): EditorialEmailNotificationOptOut
    {
        return $this->data;
    }
}