<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\SiteNotification\SiteNotification;

class SiteNotifyCommand
{
    private SiteNotification $data;

    public function __construct(SiteNotification $data)
    {
        $this->data = $data;
    }

    public function getData(): SiteNotification
    {
        return $this->data;
    }
}