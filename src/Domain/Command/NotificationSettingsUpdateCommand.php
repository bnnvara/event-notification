<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\Settings;

class NotificationSettingsUpdateCommand
{
    private Settings $data;

    public function __construct(Settings $data)
    {
        $this->data = $data;
    }

    public function getData(): Settings
    {
        return $this->data;
    }
}
