<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialBundledEmailNotification;

class EditorialNotificationSendBundledEmailCommand
{
    private EditorialBundledEmailNotification $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData(): EditorialBundledEmailNotification
    {
        return $this->data;
    }
}
