<?php

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\DoubleOptInEmailNotification;

class DoubleOptInEmailNotifyCommand
{
    private DoubleOptInEmailNotification $data;

    public function __construct(DoubleOptInEmailNotification $data)
    {
        $this->data = $data;
    }

    public function getData(): DoubleOptInEmailNotification
    {
        return $this->data;
    }
}