<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Command;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotification;

class EditorialNotificationSendEmailCommand
{
    private EditorialEmailNotification $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData(): EditorialEmailNotification
    {
        return $this->data;
    }
}