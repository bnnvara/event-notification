<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Event;

use BNNVARA\Notification\Domain\ValueObject\EditorialNotification;

class EditorialNotificationCreatedEvent
{
    private EditorialNotification $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData(): EditorialNotification
    {
        return $this->data;
    }
}