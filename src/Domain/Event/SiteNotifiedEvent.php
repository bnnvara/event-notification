<?php
declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Event;

use BNNVARA\Notification\Domain\ValueObject\SiteNotification\SiteNotification;

class SiteNotifiedEvent
{
    private SiteNotification $data;

    public function __construct(SiteNotification $data)
    {
        $this->data = $data;
    }

    public function getData(): SiteNotification
    {
        return $this->data;
    }
}