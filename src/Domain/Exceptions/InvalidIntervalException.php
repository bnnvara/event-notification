<?php

declare(strict_types=1);

namespace BNNVARA\Notification\Domain\Exceptions;

use Exception;

class InvalidIntervalException extends Exception
{

}
