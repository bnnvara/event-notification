# Changelog

All notable changes to this project will be documented in this file. Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [11.4] - 2022-05-03

### Removed

- CommentReportedNotifyCommand

## [11.3] - 2021-07-19

### Added

- PublishedAt for a Publication

## [11.2] - 2021-07-08

### Fixed

- Unknown attribute $data in EditorialNotificationSendForAccountCommand

## [11.1] - 2021-07-06

### Added

- EditorialNotificationSendForAccountCommand

## [11.0] - 2021-06-29

### Added

- EditorialBundledEmailNotification

## [10.1] - 2021-06-29

### Added

- Settings updated with interval

## [10.0] - 2021-06-08

### Removed

- NotificationSettingsUpdatedEvent

## [9.1] - 2021-06-08

### Added

- NotificationSettingsUpdateCommand

## [9.0] - 2021-06-07

### Changed

- Refactor of NotificationSettingsUpdatedEvent

## [8.0] - 2021-05-31

### Added

- NotificationSettingsUpdatedEvent

## [7.3] - 2021-05-25

### Added

- PHP8 support

## [7.2] - 2021-05-25

### Added

- slug and brandSlug to EditorialNotification and EditorialEmailNotification
- .editorconfig

### Removed

- link from EditorialNotification and EditorialEmailNotification

## [7.1] - 2021-05-25

### Added

- accountEmailOptOutSecret to EditorialEmailNotification

## [7.0] - 2021-05-19

### Added

- EditorialEmailNotificationOptOutCommand

## [6.3] - 2021-04-20

### Added

- brandName to EditorialNotification

## [6.2] - 2021-04-15

### Added

- imageUrl, content, link and linkText to EditorialNotification

## [6.1] - 2021-04-14

### Changed

- Make use of parent EmailNotification

## [6.0] - 2021-04-14

### Added

- EditorialNotificationSendEmailCommand
- EditorialNotificationCreatedEvent

## [5.0] - 2020-11-02

### Changed

- Placed the array for supported notification types in the private method to avoid serialisation issues.

## [4.0] - 2020-11-02

### Added

- Support for MarkAllAsReadCommand, that marks all the notifications from a specific user and platform as read.

### Changed

- Fixed some namespacing, placing AccountId back to parent folder, breaking backwards compatibility.

## [3.1] - 2020-11-02

### Added

- added support for MediaCreatedEvent

## [3.0] - 2020-10-14

### Added

- added datetime to the eventnotification object

## [2.1] - 2020-09-22

### Added

- BrandName in CommentReported

## [2.0] - 2020-09-21

### Added

- Site Notify Command & Site Notified Event for notifications.

### Changed

- Refactored folder structure for more overview.

## [1.3] - 2020-09-21

### Added

- Source URL field to CommentReportedNotifyCommand.

## [1.2] - 2020-09-01

### Added

- flagged comment Notification

## [1.1] - 2020-08-20

### Changed

- Uuid is now a mandatory field for the VerifyEmailNotification.

## [1.0] - 2020-08-20

### Added

- Support for the Email Address verification notification.

## [0.5.1] - 2020-05-17

### Changed

- hotfix: made AccountId nullable for lite accounts.

## [0.5] - 2020-05-04

### Added

- AccountId for validation mails.

## [0.4] - 2020-05-04

### Added

- SubscriptionToken for validation mails.

## [0.3] - 2020-05-01

### Changed

- More stuff that had to be changed in order for the packagist to work. (changed all the namespaces.)

## [0.2.1] - 2020-05-01

### Changed

- Versionnumber because of Issues with packagist.

## [0.1] - 2020-04-30

### Added

- EmailNotification Abstract for delegator support in packages.
- DoubleOptInEmailNotification for double opt-in emails to people who subscribe to newsletters.
- This Package & Changelog