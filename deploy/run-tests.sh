#!/bin/bash

apt-get update && apt-get install -y git unzip libzip-dev zip
docker-php-ext-install bcmath zip
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install
export ENV=dev

vendor/bin/phpunit --stop-on-failure

