<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\ValueObject\Publication;
use DateTime;
use PHPUnit\Framework\TestCase;

class PublicationTest extends TestCase
{
    /** @test */
    public function aPublicationCanBeCreated(): void
    {
        $publishedAt = new DateTime();

        $publication = new Publication(
            'titel',
            'sluggetje',
            'https://media.giphy.com/media/gVE2hrmFMmX7O/giphy.gif',
            'We gaan loessoe',
            'klik hier dan jonguh',
            'kassa',
            'kassa-slug',
            $publishedAt
        );

        $this->assertSame('titel', $publication->getTitle());
        $this->assertSame('sluggetje', $publication->getSlug());
        $this->assertSame('https://media.giphy.com/media/gVE2hrmFMmX7O/giphy.gif', $publication->getImageUrl());
        $this->assertSame('We gaan loessoe', $publication->getContent());
        $this->assertSame('klik hier dan jonguh', $publication->getLinkText());
        $this->assertSame('kassa', $publication->getBrandName());
        $this->assertSame('kassa-slug', $publication->getBrandSlug());
        $this->assertSame($publishedAt, $publication->getPublishedAt());
    }
}
