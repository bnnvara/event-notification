<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\ValueObject\EditorialNotification;
use PHPUnit\Framework\TestCase;

class EditorialNotificationTest extends TestCase
{
    /** @test */
    public function anEditorialNotificationSendCanBeCreated(): void
    {
        $notification = new EditorialNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            'test',
            'cool-slug',
            'image.url',
            'content of the message',
            'klik hier voor meer info',
            'kassa',
            'awesome-brand-slug'
        );

        $this->assertInstanceOf(EditorialNotification::class, $notification);
        $this->assertSame('b5844322-8532-43f6-acfc-afcba35432b1', $notification->getUserId());
        $this->assertSame('test', $notification->getTitle());
        $this->assertSame('cool-slug', $notification->getSlug());
        $this->assertSame('image.url', $notification->getImageUrl());
        $this->assertSame('content of the message', $notification->getContent());
        $this->assertSame('klik hier voor meer info', $notification->getLinkText());
        $this->assertSame('kassa', $notification->getBrandName());
        $this->assertSame('awesome-brand-slug', $notification->getBrandSlug());
    }
}
