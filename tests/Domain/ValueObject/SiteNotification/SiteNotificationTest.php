<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject\SiteNotification;

use BNNVARA\Notification\Domain\ValueObject\AccountId;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\JsonPayload;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\Name;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\SiteNotification;
use DateTime;
use PHPUnit\Framework\TestCase;

class SiteNotificationTest extends TestCase
{
    /** @test */
    public function aSiteNotificationCanBeCreated(): void
    {
        $notification = new SiteNotification(
            new AccountId('12345678-1234-1234-1234-123456789012'),
            new Name('SubscribedToBrand'),
            \DateTime::createFromFormat("d/m/Y H:i:s", "14/10/2020 14:48:21"),
            new JsonPayload('[]')
        );

        $this->assertInstanceOf(SiteNotification::class, $notification);
        $this->assertInstanceOf(AccountId::class, $notification->getAccountId());
        $this->assertInstanceOf(Name::class, $notification->getName());
        $this->assertInstanceOf(DateTime::class, $notification->getDateTime());
        $this->assertInstanceOf(JsonPayload::class, $notification->getPayload());
    }
}