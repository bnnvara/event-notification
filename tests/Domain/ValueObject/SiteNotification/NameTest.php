<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject\SiteNotification;

use BNNVARA\Notification\Domain\ValueObject\SiteNotification\Name;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    /** @test */
    public function aSubscribedToBrandCanBeCreated(): void
    {
        $name = new Name('SubscribedToBrand');

        $this->assertInstanceOf(Name::class, $name);
        $this->assertSame(Name::NAME_BRAND_SUBSCRIBED, (string) $name);
    }

    /** @test */
    public function anArticleCreatedCanBeCreated(): void
    {
        $name = new Name('ArticleCreated');

        $this->assertInstanceOf(Name::class, $name);
        $this->assertSame(Name::ARTICLE_CREATED, (string) $name);
    }

    /** @test */
    public function aMediaCreatedCanBeCreated(): void
    {
        $name = new Name('MediaCreated');

        $this->assertInstanceOf(Name::class, $name);
        $this->assertSame(Name::MEDIA_CREATED, (string) $name);
    }

    /** @test */
    public function anInvalidArgumentExceptionIsThrownWhenAnInvalidNameIsProvided(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Name('BrandFollowed');
    }
}