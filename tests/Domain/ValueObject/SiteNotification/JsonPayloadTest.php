<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\ValueObject\SiteNotification\JsonPayload;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class JsonPayloadTest extends TestCase
{
    /** @test
     *  @dataProvider getValidPayloadData
     */
    public function aJsonPayloadCanBeCreated(string $payloadData): void
    {
        $payload = new JsonPayload($payloadData);

        $this->assertInstanceOf(JsonPayload::class, $payload);
        $this->assertSame($payloadData, (string) $payload);
    }

    /** @test
     *  @dataProvider getInvalidPayloadData
     */
    public function anInvalidArgumentExceptionIsThrownOnInvalidData(string $payloadData): void
    {
        $this->expectException(InvalidArgumentException::class);

        new JsonPayload($payloadData);
    }

    public function getValidPayloadData(): array
    {
        return [
            [
                '[]'
            ],
            [
                '[{"test": "test"}]'
            ],
            [
                '{"test": "test"}'
            ]
        ];
    }

    public function getInvalidPayloadData(): array
    {
        return [
            [
                'foobar'
            ],
            [
                '[{"test": "test}]'
            ],
            [
                '{"test: "test"}'
            ],
            [
                ''
            ]
        ];
    }
}