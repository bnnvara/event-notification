<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotificationOptOut;
use PHPUnit\Framework\TestCase;

class EditorialEmailNotificationOptOutTest extends TestCase
{
    /** @test */
    public function anEditorialEmailNotificationOptOutCanBeCreated(): void
    {
        $editorialEmailNotificationOptOut = new EditorialEmailNotificationOptOut('6590008E66D7EB80F705B98F8E405859A7B391E98C5687345DE84686CC055054');

        $this->assertInstanceOf(EditorialEmailNotificationOptOut::class, $editorialEmailNotificationOptOut);
        $this->assertSame('6590008E66D7EB80F705B98F8E405859A7B391E98C5687345DE84686CC055054', $editorialEmailNotificationOptOut->getHash());
    }
}