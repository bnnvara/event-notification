<?php

namespace BNNVARA\Tests\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\DoubleOptInEmailNotification;
use PHPUnit\Framework\TestCase;

class DoubleOptInEmailNotificationTest extends TestCase
{
    /** @test */
    public function aDoubleOptInEmailNotificationCanBeCreated(): void
    {
        $notification = new DoubleOptInEmailNotification(
            'accountId',
            'test@example.com',
            'DWDD',
            '345i34h5l34llh434kkei9g8dhe934g83'
        );
        $this->assertInstanceOf(DoubleOptInEmailNotification::class, $notification);
        $this->assertEquals('accountId', $notification->getAccountId());
        $this->assertEquals('test@example.com', $notification->getEmailAddress());
        $this->assertEquals('DWDD', $notification->getSubscriptionListName());
        $this->assertEquals('DoubleOptIn', $notification->getType());
        $this->assertEquals('345i34h5l34llh434kkei9g8dhe934g83', $notification->getSubscriptionToken());
    }

    /** @test */
    public function aDoubleOptInEmailNotificationWithNoAccountIdCanBeCreated(): void
    {
        $notification = new DoubleOptInEmailNotification(
            null,
            'test@example.com',
            'DWDD',
            '345i34h5l34llh434kkei9g8dhe934g83'
        );
        $this->assertInstanceOf(DoubleOptInEmailNotification::class, $notification);
        $this->assertNull($notification->getAccountId());
        $this->assertEquals('test@example.com', $notification->getEmailAddress());
        $this->assertEquals('DWDD', $notification->getSubscriptionListName());
        $this->assertEquals('DoubleOptIn', $notification->getType());
        $this->assertEquals('345i34h5l34llh434kkei9g8dhe934g83', $notification->getSubscriptionToken());
    }
}