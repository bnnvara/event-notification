<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialBundledEmailNotification;
use BNNVARA\Notification\Domain\ValueObject\Publication;
use DateTime;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Test;
use TypeError;

class EditorialBundledEmailNotificationTest extends TestCase
{
    /** @test */
    public function anEditorialBundledEmailNotificationSendCanBeCreated(): void
    {
        $publications = $this->getPublicationsArray();
        $notification = new EditorialBundledEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            $publications,
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );

        $this->assertInstanceOf(EditorialBundledEmailNotification::class, $notification);
        $this->assertSame('b5844322-8532-43f6-acfc-afcba35432b1', $notification->getUserId());
        $this->assertSame($publications, $notification->getPublications());
        $this->assertSame(
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            $notification->getAccountEmailOptOutSecret()
        );
        $this->assertSame('test@bnnvara.nl', $notification->getEmailAddress());
    }

    /** @test */
    public function anEditorialBundledEmailNotificationSendThrowsAnErrorWhenAStringAndNotAPublicationIsAdded(): void
    {
        $this->expectException(TypeError::class);
        new EditorialBundledEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            $this->getPublicationsArrayWithAWrongType(),
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );
    }

    /** @test */
    public function anEditorialBundledEmailNotificationSendThrowsAnErrorWhenANotPublicationIsAdded(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new EditorialBundledEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            $this->getPublicationsArrayWithAWrongClass(),
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );
    }

    private function getPublicationsArray(): array
    {
        return [
            new Publication(
                'titel',
                'sluggetje',
                'https://media.giphy.com/media/gVE2hrmFMmX7O/giphy.gif',
                'We gaan loessoe',
                'klik hier dan jonguh',
                'kassa',
                'kassa-slug',
                new DateTime()
            ),
            new Publication(
                'titel 2',
                'sluggetje-2',
                'https://media.giphy.com/media/uWA0mkEUq6tcAjeCTy/giphy.gif',
                'wat is Bomboclat',
                'klik hier dan drerrie',
                'kvk',
                'kvk-slug',
                new DateTime()
            )
        ];
    }

    private function getPublicationsArrayWithAWrongType(): array
    {
        return [
            'toet',
            new Publication(
                'titel 2',
                'sluggetje-2',
                'https://media.giphy.com/media/uWA0mkEUq6tcAjeCTy/giphy.gif',
                'wat is Bomboclat',
                'klik hier dan drerrie',
                'kvk',
                'kvk-slug',
                new DateTime()
            )
        ];
    }

    private function getPublicationsArrayWithAWrongClass(): array
    {
        return [
            new Test(),
            new Publication(
                'titel 2',
                'sluggetje-2',
                'https://media.giphy.com/media/uWA0mkEUq6tcAjeCTy/giphy.gif',
                'wat is Bomboclat',
                'klik hier dan drerrie',
                'kvk',
                'kvk-slug',
                new DateTime()
            )
        ];
    }
}
