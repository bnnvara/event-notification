<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotification;
use PHPUnit\Framework\TestCase;

class EditorialEmailNotificationTest extends TestCase
{
    /** @test */
    public function anEditorialEmailNotificationSendCanBeCreated(): void
    {
        $notification = new EditorialEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            'test',
            'awesome-slug',
            'image.url',
            'content of the message',
            'klik hier voor meer info',
            'kassa',
            'cool-brand-slug',
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );

        $this->assertInstanceOf(EditorialEmailNotification::class, $notification);
        $this->assertSame('b5844322-8532-43f6-acfc-afcba35432b1', $notification->getUserId());
        $this->assertSame('test', $notification->getTitle());
        $this->assertSame('awesome-slug', $notification->getSlug());
        $this->assertSame('image.url', $notification->getImageUrl());
        $this->assertSame('content of the message', $notification->getContent());
        $this->assertSame('klik hier voor meer info', $notification->getLinkText());
        $this->assertSame('kassa', $notification->getBrandName());
        $this->assertSame('cool-brand-slug', $notification->getBrandSlug());
        $this->assertSame(
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            $notification->getAccountEmailOptOutSecret()
        );
        $this->assertSame('test@bnnvara.nl', $notification->getEmailAddress());
    }
}
