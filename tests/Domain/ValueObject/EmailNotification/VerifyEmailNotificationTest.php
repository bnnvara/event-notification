<?php

namespace BNNVARA\Tests\Notification\Domain\ValueObject\EmailNotification;

use BNNVARA\Notification\Domain\ValueObject\EmailNotification\VerifyEmailNotification;
use PHPUnit\Framework\TestCase;

class VerifyEmailNotificationTest extends TestCase
{
    /** @test */
    public function aVerifyEmailNotificationCanBeCreated(): void
    {
        $notification = new VerifyEmailNotification(
            '12345678-1234-1234-1234-123456789012',
            'email@example.com',
            '235234523452345234523453264'
        );

        $this->assertSame('12345678-1234-1234-1234-123456789012', $notification->getUuid());
        $this->assertSame('email@example.com', $notification->getEmailAddress());
        $this->assertSame('VerifyEmail', $notification->getType());
        $this->assertSame('235234523452345234523453264', $notification->getVerificationToken());
    }
}