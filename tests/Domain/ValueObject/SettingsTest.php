<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\Exceptions\InvalidIntervalException;
use BNNVARA\Notification\Domain\ValueObject\Settings;
use PHPUnit\Framework\TestCase;

class SettingsTest extends TestCase
{
    /**
     * @test
     * @dataProvider validSettingsProvider
     */
    public function aSettingsCanBeCreated(string $accountIdValue, bool $editorialNotificationPerEmailValue, string $editorialNotificationInterval): void
    {
        $settings = new Settings($accountIdValue, $editorialNotificationPerEmailValue, $editorialNotificationInterval);

        $this->assertInstanceOf(Settings::class, $settings);
        $this->assertSame($accountIdValue, $settings->getAccountId());
        $this->assertSame($editorialNotificationPerEmailValue, $settings->hasEditorialNotificationPerEmail());
    }

    public function validSettingsProvider(): array
    {
        return [
            [
                '76efa765-9dac-4cd9-8c07-6a0e7b6a42c6',
                false,
                'None'
            ],
            [
                'f233346e-173b-4807-b6c0-a10256c3744b',
                true,
                'Daily'
            ]
        ];
    }

    /** @test */
    public function itThrowsAnErrorWhenInvalidIntervalIsGiven(): void
    {
        $this->expectException(InvalidIntervalException::class);
        new Settings('76efa765-9dac-4cd9-8c07-6a0e7b6a42c6', false, 'wumbo');
    }
}
