<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\ValueObject\DistributionPlatform;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DistributionPlatformTest extends TestCase
{
    /** @test */
    public function aSiteNotificationDistributionPlatformCanBeCreated(): void
    {
        $platform = new DistributionPlatform(DistributionPlatform::PLATFORM_SITE_NOTIFICATION);
        $this->assertInstanceOf(DistributionPlatform::class, $platform);
        $this->assertSame('SiteNotification', (string) $platform);
    }

    /** @test */
    public function anEmailDistributionPlatformCanBeCreated(): void
    {
        $platform = new DistributionPlatform(DistributionPlatform::PLATFORM_EMAIL_NOTIFICATION);
        $this->assertInstanceOf(DistributionPlatform::class, $platform);
        $this->assertSame('EmailNotification', (string) $platform);
    }

    /** @test */
    public function anInvalidArgumentExceptionIsThrownOnInvalidArgument(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new DistributionPlatform('bla');
    }
}