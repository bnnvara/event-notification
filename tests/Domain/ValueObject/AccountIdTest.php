<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\ValueObject;

use BNNVARA\Notification\Domain\ValueObject\AccountId;
use PHPUnit\Framework\TestCase;

class AccountIdTest extends TestCase
{
    /** @test */
    public function anAccountIdCanBeCreated(): void
    {
        $accountId = new AccountId('test');

        $this->assertInstanceOf(AccountId::class, $accountId);
        $this->assertEquals('test', (string) $accountId);
    }
}