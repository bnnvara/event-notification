<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Event;

use BNNVARA\Notification\Domain\Event\SiteNotifiedEvent;
use BNNVARA\Notification\Domain\ValueObject\AccountId;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\JsonPayload;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\Name;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\SiteNotification;
use PHPUnit\Framework\TestCase;

class SiteNotifiedEventTest extends TestCase
{
    /** @test */
    public function aSiteNotifiedEventCanBeCreated(): void
    {
        $notification = new SiteNotification(
            new AccountId('12345678-1234-1234-1234-123456789012'),
            new Name('SubscribedToBrand'),
            \DateTime::createFromFormat("d/m/Y H:i:s", "14/10/2020 14:48:21"),
            new JsonPayload('[]')
        );

        $event = new SiteNotifiedEvent($notification);

        $this->assertInstanceOf(SiteNotifiedEvent::class, $event);
        $this->assertInstanceOf(SiteNotification::class, $event->getData());
    }
}