<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Event;

use BNNVARA\Notification\Domain\Event\EditorialNotificationCreatedEvent;
use BNNVARA\Notification\Domain\ValueObject\EditorialNotification;
use PHPUnit\Framework\TestCase;

class EditorialNotificationCreatedEventTest extends TestCase
{
    /** @test */
    public function anEditorialNotificationCreatedEventCanBeCreated(): void
    {
        $editorialNotificationCreatedEvent = new EditorialNotificationCreatedEvent($this->getEditorialNotification());

        $this->assertInstanceOf(EditorialNotificationCreatedEvent::class, $editorialNotificationCreatedEvent);
        $this->assertInstanceOf(EditorialNotification::class, $editorialNotificationCreatedEvent->getData());
    }

    private function getEditorialNotification(): EditorialNotification
    {
        return new EditorialNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            'test',
            'test-slug',
            'image.url',
            'content of the message',
            'klik hier voor meer info',
            'kassa',
            'kassa-slug'
        );
    }
}
