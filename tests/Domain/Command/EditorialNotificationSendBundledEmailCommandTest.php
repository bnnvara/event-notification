<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\EditorialNotificationSendBundledEmailCommand;
use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialBundledEmailNotification;
use BNNVARA\Notification\Domain\ValueObject\Publication;
use DateTime;
use PHPUnit\Framework\TestCase;

class EditorialNotificationSendBundledEmailCommandTest extends TestCase
{
    /** @test */
    public function anEditorialNotificationSendBundledEmailCommandCanBeCreated(): void
    {
        $editorialNotificationSendBundledCommand = new EditorialNotificationSendBundledEmailCommand(
            $this->getBundledEditorialNotification()
        );

        $this->assertInstanceOf(
            EditorialNotificationSendBundledEmailCommand::class,
            $editorialNotificationSendBundledCommand
        );
        $this->assertInstanceOf(
            EditorialBundledEmailNotification::class,
            $editorialNotificationSendBundledCommand->getData()
        );
    }

    private function getBundledEditorialNotification(): EditorialBundledEmailNotification
    {
        return new EditorialBundledEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            [
                new Publication(
                    'titel',
                    'sluggetje',
                    'https://media.giphy.com/media/gVE2hrmFMmX7O/giphy.gif',
                    'We gaan loessoe',
                    'klik hier dan jonguh',
                    'kassa',
                    'kassa-slug',
                    new DateTime()
                )
            ],
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );
    }
}
