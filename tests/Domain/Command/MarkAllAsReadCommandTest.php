<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\MarkAllAsReadCommand;
use BNNVARA\Notification\Domain\ValueObject\AccountId;
use BNNVARA\Notification\Domain\ValueObject\DistributionPlatform;
use PHPUnit\Framework\TestCase;

class MarkAllAsReadCommandTest extends TestCase
{
    /** @test */
    public function aMarkAllAsReadCommandCanBeCreated(): void
    {
        $platform = new DistributionPlatform(DistributionPlatform::PLATFORM_SITE_NOTIFICATION);
        $accountId = new AccountId('test');
        $command = new MarkAllAsReadCommand($accountId, $platform);

        $this->assertInstanceOf(MarkAllAsReadCommand::class, $command);
        $this->assertInstanceOf(AccountId::class, $command->getAccountId());
        $this->assertInstanceOf(DistributionPlatform::class, $command->getDistributionPlatform());
    }
}