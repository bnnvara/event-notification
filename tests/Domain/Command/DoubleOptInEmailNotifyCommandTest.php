<?php

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\DoubleOptInEmailNotifyCommand;
use BNNVARA\Notification\Domain\ValueObject\EmailNotification\DoubleOptInEmailNotification;
use PHPUnit\Framework\TestCase;

class DoubleOptInEmailNotifyCommandTest extends TestCase
{

    /** @test */
    public function aDoubleOptInEmailNotifyCommandCanBeCreated(): void
    {
        $notification = new DoubleOptInEmailNotification(
            'accountId',
            'test@example.com',
            'DWDD',
            'token'
        );

        $command = new DoubleOptinEmailNotifyCommand($notification);

        $this->assertInstanceOf(DoubleOptInEmailNotifyCommand::class, $command);
        $this->assertEquals('accountId', $command->getData()->getAccountId());
        $this->assertEquals('test@example.com', $command->getData()->getEmailAddress());
        $this->assertEquals('DoubleOptIn', $command->getData()->getType());
        $this->assertEquals('DWDD', $command->getData()->getSubscriptionListName());
        $this->assertEquals('token', $command->getData()->getSubscriptionToken());
    }
}