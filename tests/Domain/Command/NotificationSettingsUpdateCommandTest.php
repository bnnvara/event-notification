<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\NotificationSettingsUpdateCommand;
use BNNVARA\Notification\Domain\ValueObject\Settings;
use PHPUnit\Framework\TestCase;

class NotificationSettingsUpdateCommandTest extends TestCase
{
    /**
     * @test
     */
    public function aNotificationSettingsUpdateCommandCanBeCreated(): void
    {
        $settings = $this->getSettings();

        $command = new NotificationSettingsUpdateCommand($settings);

        $this->assertInstanceOf(NotificationSettingsUpdateCommand::class, $command);
        $this->assertInstanceOf(Settings::class, $command->getData());
    }

    private function getSettings(): Settings
    {
        return new Settings('76efa765-9dac-4cd9-8c07-6a0e7b6a42c6', false, 'Daily');
    }
}
