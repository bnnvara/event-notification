<?php

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\VerifyEmailNotifyCommand;
use BNNVARA\Notification\Domain\ValueObject\EmailNotification\VerifyEmailNotification;
use PHPUnit\Framework\TestCase;

class VerifyEmailNotifyCommandTest extends TestCase
{
    /** @test */
    public function aVerifyEmailNotifyCommandCanBeCreated(): void
    {
        $notification = new VerifyEmailNotification(
            '12345678-1234-1234-1234-123456789012',
            'email@example.com',
            '235234523452345234523453264'
        );

        $command = new VerifyEmailNotifyCommand($notification);

        $this->assertSame($notification, $command->getData());
    }
}