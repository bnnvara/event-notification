<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\EditorialNotificationSendEmailCommand;
use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotification;
use PHPUnit\Framework\TestCase;

class EditorialNotificationSendEmailCommandTest extends TestCase
{
    /** @test */
    public function anEditorialNotificationSendEmailCommandCanBeCreated(): void
    {
        $editorialNotificationSendCommand = new EditorialNotificationSendEmailCommand(
            $this->getEditorialNotification()
        );

        $this->assertInstanceOf(EditorialNotificationSendEmailCommand::class, $editorialNotificationSendCommand);
        $this->assertInstanceOf(EditorialEmailNotification::class, $editorialNotificationSendCommand->getData());
    }

    private function getEditorialNotification(): EditorialEmailNotification
    {
        return new EditorialEmailNotification(
            'b5844322-8532-43f6-acfc-afcba35432b1',
            'test',
            'sluggg',
            'image.url',
            'content of the message',
            'klik hier voor meer info',
            'kassa',
            'kassa-slugg',
            '3302A2D38B27DE8CC464413B0DD175BD5D51BB13803E7E6FA79332E5484B0AB7',
            'test@bnnvara.nl'
        );
    }

}
