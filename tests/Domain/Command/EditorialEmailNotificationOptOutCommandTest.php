<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\EditorialEmailNotificationOptOutCommand;
use BNNVARA\Notification\Domain\ValueObject\EmailNotification\EditorialEmailNotificationOptOut;
use PHPUnit\Framework\TestCase;

class EditorialEmailNotificationOptOutCommandTest extends TestCase
{
    /** @test */
    public function anEditorialEmailNotificationOptOutCommandCanBeCreated(): void
    {
        $editorialEmailNotificationOptOutCommand = new EditorialEmailNotificationOptOutCommand(
            new EditorialEmailNotificationOptOut(
                '6590008E66D7EB80F705B98F8E405859A7B391E98C5687345DE84686CC055054'
            )
        );

        $this->assertInstanceOf(EditorialEmailNotificationOptOutCommand::class, $editorialEmailNotificationOptOutCommand);
        $this->assertInstanceOf(EditorialEmailNotificationOptOut::class, $editorialEmailNotificationOptOutCommand->getData());
        $this->assertSame('6590008E66D7EB80F705B98F8E405859A7B391E98C5687345DE84686CC055054', $editorialEmailNotificationOptOutCommand->getData()->getHash());
    }
}