<?php
declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\SiteNotifyCommand;
use BNNVARA\Notification\Domain\ValueObject\AccountId;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\JsonPayload;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\Name;
use BNNVARA\Notification\Domain\ValueObject\SiteNotification\SiteNotification;
use PHPUnit\Framework\TestCase;

class SiteNotifyCommandTest extends TestCase
{
    /** @test */
    public function aSiteNotifyCommandCanBeCreated(): void
    {
        $notification = new SiteNotification(
            new AccountId('12345678-1234-1234-1234-123456789012'),
            new Name('SubscribedToBrand'),
            \DateTime::createFromFormat("d/m/Y H:i:s", "14/10/2020 14:48:21"),
            new JsonPayload('[]')
        );

        $command = new SiteNotifyCommand($notification);

        $this->assertInstanceOf(SiteNotifyCommand::class, $command);
        $this->assertInstanceOf(SiteNotification::class, $command->getData());
    }
}