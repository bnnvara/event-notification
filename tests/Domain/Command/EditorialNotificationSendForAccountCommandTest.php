<?php

declare(strict_types=1);

namespace BNNVARA\Tests\Notification\Domain\Command;

use BNNVARA\Notification\Domain\Command\EditorialNotificationSendForAccountCommand;
use BNNVARA\Notification\Domain\ValueObject\Settings;
use PHPUnit\Framework\TestCase;

class EditorialNotificationSendForAccountCommandTest extends TestCase
{
    /** @test */
    public function anEditorialNotificationSendForAccountCommandCanBeCreated(): void
    {
        $settings = new Settings(
            'asd',
            true,
            'Daily'
        );
        $sendCommand = new EditorialNotificationSendForAccountCommand(
            $settings
        );

        $this->assertInstanceOf(EditorialNotificationSendForAccountCommand::class, $sendCommand);
        $this->assertInstanceOf(Settings::class, $sendCommand->getData());
        $this->assertSame($settings, $sendCommand->getData());
    }
}
